// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Sun Mar 24 03:01:29 2019
// Host        : DESKTOP-9R3R9UN running 64-bit major release  (build 9200)
// Command     : write_verilog -mode funcsim -nolib -force -file
//               G:/ProjectFiles/Xilinx/VendingFSMTFF/VendingFSMTFF.sim/sim_1/synth/func/xsim/VendingMachine_tb_func_synth.v
// Design      : VendingMachine
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module TFlipFlop
   (Z_OBUF,
    subClk,
    a_IBUF,
    rst_IBUF);
  output Z_OBUF;
  input subClk;
  input a_IBUF;
  input rst_IBUF;

  wire Z_OBUF;
  wire a_IBUF;
  wire rst_IBUF;
  wire subClk;
  wire temp_reg_C_n_0;
  wire temp_reg_LDC_i_1_n_0;
  wire temp_reg_LDC_i_2_n_0;
  wire temp_reg_LDC_n_0;
  wire temp_reg_P_n_0;

  LUT3 #(
    .INIT(8'hB8)) 
    Z_OBUF_inst_i_1
       (.I0(temp_reg_P_n_0),
        .I1(temp_reg_LDC_n_0),
        .I2(temp_reg_C_n_0),
        .O(Z_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    temp_reg_C
       (.C(subClk),
        .CE(1'b1),
        .CLR(temp_reg_LDC_i_2_n_0),
        .D(Z_OBUF),
        .Q(temp_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    temp_reg_LDC
       (.CLR(temp_reg_LDC_i_2_n_0),
        .D(1'b1),
        .G(temp_reg_LDC_i_1_n_0),
        .GE(1'b1),
        .Q(temp_reg_LDC_n_0));
  LUT5 #(
    .INIT(32'h000047B8)) 
    temp_reg_LDC_i_1
       (.I0(temp_reg_P_n_0),
        .I1(temp_reg_LDC_n_0),
        .I2(temp_reg_C_n_0),
        .I3(a_IBUF),
        .I4(rst_IBUF),
        .O(temp_reg_LDC_i_1_n_0));
  LUT5 #(
    .INIT(32'h45401015)) 
    temp_reg_LDC_i_2
       (.I0(rst_IBUF),
        .I1(temp_reg_P_n_0),
        .I2(temp_reg_LDC_n_0),
        .I3(temp_reg_C_n_0),
        .I4(a_IBUF),
        .O(temp_reg_LDC_i_2_n_0));
  FDPE #(
    .INIT(1'b1)) 
    temp_reg_P
       (.C(subClk),
        .CE(1'b1),
        .D(Z_OBUF),
        .PRE(temp_reg_LDC_i_1_n_0),
        .Q(temp_reg_P_n_0));
endmodule

(* COUNT_MAX = "0" *) 
(* NotValidForBitStream *)
module VendingMachine
   (a,
    b,
    rst,
    clk,
    Z,
    ZA,
    ZB,
    ZC);
  input a;
  input b;
  input rst;
  input clk;
  output Z;
  output ZA;
  output ZB;
  output ZC;

  wire Z;
  wire ZA;
  wire ZB;
  wire ZC;
  wire Z_OBUF;
  wire a;
  wire a_IBUF;
  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire rst;
  wire rst_IBUF;
  wire subClk;
  wire subClk_i_1_n_0;

  OBUFT ZA_OBUF_inst
       (.I(1'b0),
        .O(ZA),
        .T(1'b1));
  OBUFT ZB_OBUF_inst
       (.I(1'b0),
        .O(ZB),
        .T(1'b1));
  OBUF ZC_OBUF_inst
       (.I(1'b1),
        .O(ZC));
  OBUF Z_OBUF_inst
       (.I(Z_OBUF),
        .O(Z));
  IBUF a_IBUF_inst
       (.I(a),
        .O(a_IBUF));
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  IBUF rst_IBUF_inst
       (.I(rst),
        .O(rst_IBUF));
  LUT1 #(
    .INIT(2'h1)) 
    subClk_i_1
       (.I0(subClk),
        .O(subClk_i_1_n_0));
  FDRE #(
    .INIT(1'b1)) 
    subClk_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(subClk_i_1_n_0),
        .Q(subClk),
        .R(1'b0));
  TFlipFlop tff2
       (.Z_OBUF(Z_OBUF),
        .a_IBUF(a_IBUF),
        .rst_IBUF(rst_IBUF),
        .subClk(subClk));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
