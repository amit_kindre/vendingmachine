
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.03.2019 19:54:00
// Design Name: 
// Module Name: VendingMachine_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module VendingMachine_tb();
    reg a;
    reg b;
    reg clk;
    reg rst;
    wire Z;
    wire ZA;
    wire ZB;
    wire ZC;
   
    
    VendingMachine test_ckt(
    .a(a),
    .b(b),
    .clk(clk),
    .rst(rst),
    .Z(Z),
    .ZA(ZA),
    .ZB(ZB),
    .ZC(ZC));
  
    
    
    
    always #5 clk = !clk;
    
    initial begin
   
    a = 0;
    b = 0;
    clk = 0;
    rst = 0;
    rst = 1;
    #10 rst = 0;
    
    
    
    a = 1;
    b = 1;
    #5
    a = 1;
    b = 1;
    #5
    a = 1;
    b = 1;
    #5
    a = 1;
    b = 1;
    #5
    a = 1;
    b = 1;
    #5
    a = 1;
    b = 1;
    #5
    a = 1;
    b = 1;
    #5
    a = 1;
    b = 1;
    #5
    a = 1;
    b = 1;
        
    end
    
     
endmodule
