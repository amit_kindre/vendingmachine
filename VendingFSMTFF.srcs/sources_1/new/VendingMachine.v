`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// Create Date: 11.03.2019 00:58:03
// Design Name: 
// Module Name: VendingMachine
//////////////////////////////////////////////////////////////////////////////////
module TFlipFlop (t,clkt,rst, q,qb);
    input t,clkt,rst;
    output q,qb;
    reg q,qb;
    reg temp=0;
    always@(posedge clkt,posedge rst) begin
        if (rst==0) begin
            if(t==1) begin
                temp=~ temp;
            end
            else begin 
                temp=temp; 
            end
        end
        else begin    
            temp = 0;
        end
        q=temp;qb=~temp;
    end
endmodule


module VendingMachine(
    input wire a,
    input wire b,
    input wire rst,
    input wire clk,
    
    output Z,
    
    //ZA/ZB/ZC are only for debug purpose
    output wire ZA, // Represents Sw0
    output wire ZB, // Represents Sw1
    output wire ZC  // Clock Output
);
    wire t2,t1,t0;
    wire q2n,q1n,q0n;
    wire q2,q1,q0;
    reg [31:0] cnt;
    reg subClk;
    reg Z;
    
    parameter COUNT_MAX = 32'd100000000;
    
    initial begin
        subClk = 1;
    end
      
    //Sub clock block for delay
    always @ (posedge clk) begin 
        if(cnt < COUNT_MAX)begin
               cnt <= cnt + 1;
           end
           else begin
               cnt <= 32'd0;
               subClk = !subClk;   
           end
    end
    
    TFlipFlop tff2(t2,subClk,rst,q2,q2n);    
    TFlipFlop tff1(t1,subClk,rst,q1,q1n);
    TFlipFlop tff0(t0,subClk,rst,q0,q0n);
    

    assign t0 = (q2 & q0) |   (~a & b & q2n) | (~a & b & q1n) | (b & q2n & q0n) | (a & q2 & q1n);
    assign t1 = (q2 & q1) | (a & q1) | ( a & ~b & q2n) | (a & q2n & q0n) | (~a & b & q2n & q0);
    assign t2 = (q2 & q0 ) | (q2 & q1) | (a & q1) | (b & q1 & q0) | (a & b & q0);   
    
    
    always @ (posedge subClk)
    begin
        Z = (q2 & q1n & q0);
    end
    
    //ZA/ZB/ZC are only for debug purpose
    assign ZA = a;
    assign ZB = b;
    assign ZC = subClk;
    
 endmodule

